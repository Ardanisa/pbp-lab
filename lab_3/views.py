from django.shortcuts import redirect, render
from .forms import FriendForm
from .models import Friend
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all().values
    response = {'friends' : friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
    form = FriendForm
    if request.method == 'POST':
        friend_Form = FriendForm(request.POST)
        if friend_Form.is_valid():
            friend_Form.save()
            return redirect('/lab-3')

    response = {'form' : form}
    return render(request, 'lab3_form.html', response)


