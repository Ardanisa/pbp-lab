from lab_1.models import Friend
from django.contrib import admin
from .models import Friend

# Register your models here.
admin.site.register(Friend)
