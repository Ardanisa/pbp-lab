**Apakah perbedaan antara JSON dan XML?** <br>
JSON (Javascript Object Notation) dan XML (Extensive Markup Language) pada dasarnya adalah format berbasis teks yang dapat dibaca oleh manusia yang digunakan untuk membuat, membaca, dan membuat kode aplikasi dunia nyata. Keduanya merupakan notasi teks hierarki dan bahasa independen untuk pertukaran data. XML merupakan teknologi berorientasi dokumen yang digunakan untuk menyimpan dan mewakili data terstruktur dalam format yang dapat dengan mudah dibaca oleh manusia maupun mesin. Secara umum, XML dibuat untuk menambahkan informasi tambahan pada teks biasa. XML berfungsi untuk membawa data, bukan untuk menampilkan data. Sedangkan JSON merupakan format pertukaran data yang ringan dan sederhana untuk merepresentasikan data hierarkis yang didasarkan pada sintaks objek pada JavaScripts. JSON lebih ringan dari XML karena berorientasi pada data dan minim redundansi. Berikut perbandingan diantara keduanya:
<br>
|JSON	                               | XML |
|---                                    | ---|
|JavaScript Object Notation	           |Extensible Markup Language |
|Notasi Objek JavaScript 	           | Bahasa Markup yang dapat diperluas |
|Berdasarkan syntax JavaScript Language |Berasal dari SGML |
|Mendukung Array	                       | Tidak mendukung Array |
|Mendukung pembuatan object	           |Menggunakan struktur tag untuk mewakili data item |
|Tidak mendukung namespace	           | Mendukung namespace |
|Filenya sangat mudah dibaca dibandingkan dengan XML | Dokumennya relatif lebih sulit untuk dibaca dan ditafsirkan dibandingkan dengan JSON |
|Tidak menggunakan tag akhir	           | Memiliki tag awal dan akhir |
|Kurang aman	                           |Lebih aman daripada JSON |
|Tidak mendukung komen	               |Mendukung komen |
|Hanya mendukung UTF-8 encoding	       |Mendukung berbagai encoding |
|Akses data cepat	                   |Akses data lebih lambat dari JSON |
|JSON adalah standar terbuka yang ringan format untuk pertukaran data yang diperpanjang dari JavaScript | XML adalah bahasa markup berbasis teks yang berspesialisasi dalam transaksi bisnis ke bisnis di World Wide Web|



<br><br>**Apakah perbedaan antara HTML dan XML?** <br>
HTML (Hypertext Markup Language) digunakan untuk membuat halaman dan aplikasi web. Lebih spesifiknya, HTML digunakan untuk menampilkan data, bukan untuk mengangkut data. Hypertext mendefinisikan link antar halaman web. XML (Extensible Markup Language) juga digunakan untuk membuat halaman web dan aplikasi web, namun XML merupakan bahasa dinamis yang digunakan untuk mengangkut data, bukan menampilkan data. XML merupakan format data melalui Unicode yang dapat dengan mudah dimengerti oleh manusia dan mesin. Berikut perbandingan HTML dan XML:
<br>

|HTML	                                   | XML                                    |
|---                                        | ---                                    |
|Hypertext Markup Language	               |Extensible Markup Language              |
|Berfokus untuk melakukan penyajian data	   | Berfokus untuk melakukan transfer data |
|Didorong oleh format	                   | Didorong konten                        |
|Case Insensitive	                       | Case Sensitive                         |
|Mendukung namespace	                       | Tidak mendukung namespace              |
|Tidak strict untuk tag penutup	           | Strict untuk tag penutup               |
|Tag yang dimiliki terbatas	               | Tag tidak terbatas                     |
|Tag sudah ditentukan 	                   | Tag tidak ditentukan sebelumnya (dibuat sendiri oleh programmer) |


<br><br>**Referensi:** <br>
https://www.geeksforgeeks.org/difference-between-json-and-xml/ <br>
https://id.sawakinome.com/articles/protocols--formats/difference-between-json-and-xml-3.html <br>
https://blogs.masterweb.com/perbedaan-xml-dan-html/



