#from django import urls
from django.urls import path
from .views import xml, json, index


urlpatterns = [
    path('', index, name='index'),
    path('xml', xml, name='XML'),
    path('json', json, name = 'Json'),
]
