from django.shortcuts import render, redirect
from .forms import NoteForm
from .models import Note

from lab_4.forms import NoteForm

# Create your views here.
def index(request):
    note = Note.objects.all().values
    response = {'note' : note}
    return render(request, 'lab4_index.html', response)

def add_note(request) :
    form = NoteForm
    if request.method == 'POST':
        Note_Form = NoteForm(request.POST)
        if Note_Form.is_valid():
            Note_Form.save()
            return redirect('/lab-4')

    response = {'form' : form}
    return render(request, 'lab4_form.html', response)

def note_list(request) :
    note = Note.objects.all().values
    response = {'note' : note}
    return render(request, 'lab4_note_list.html', response)

