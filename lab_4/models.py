from django.db import models

# Create your models here.
class Note(models.Model):
    to_text = models.CharField(max_length=1000, null=True, blank=True)
    from_text = models.CharField(max_length=1000, null=True, blank=True)
    tittle_text = models.CharField(max_length=1000, null=True, blank=True)
    message_text = models.TextField(max_length=1000, null=True, blank=True)
