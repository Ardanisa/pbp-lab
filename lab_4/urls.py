from django.urls import path

from lab_3.views import index
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('add-note', views.add_note, name='add-note'),
    path('note-list', views.note_list, name='note-list'),
]