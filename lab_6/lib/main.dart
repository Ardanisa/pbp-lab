import 'package:flutter/material.dart';
import 'package:lab_6/screen/registrasiSiswa.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(
          unselectedWidgetColor: Colors.white54,
          primaryColor: Colors.purple,
          appBarTheme: AppBarTheme(color: Color.fromARGB(225, 224, 79, 204)),
          scaffoldBackgroundColor: Color.fromARGB(225, 20, 28, 68),
          inputDecorationTheme: InputDecorationTheme(
              labelStyle: TextStyle(
                  color: Color.fromARGB(225, 224, 79, 204), fontSize: 12)),
          elevatedButtonTheme: ElevatedButtonThemeData(
              style: ButtonStyle(
            backgroundColor:
                MaterialStateProperty.all(Color.fromARGB(225, 224, 79, 204)),
            padding: MaterialStateProperty.all(
                EdgeInsets.symmetric(horizontal: 80, vertical: 20)),
            shape: MaterialStateProperty.all(StadiumBorder()),
            shadowColor: MaterialStateProperty.all(Colors.purpleAccent),
          )),
        ),
        home: registrasiSiswa());
  }
}
